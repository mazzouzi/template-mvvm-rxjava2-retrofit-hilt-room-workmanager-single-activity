package com.mazapps.template.ui.main.viewmodel

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mazapps.tabesto.ui.main.viewmodel.fromJson
import com.mazapps.template.TrampolineSchedulerRule
import com.mazapps.template.api.RetrofitApiService
import com.mazapps.template.data.AppDataManager
import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.data.DataManager
import com.mazapps.template.data.db.AppDbHelper
import com.mazapps.template.data.db.DbHelper
import com.mazapps.template.data.db.dao.DaoSearch
import com.mazapps.template.data.model.Search
import com.mazapps.template.data.model.Wikipedia
import com.mazapps.template.data.network.ApiHelper
import com.mazapps.template.data.network.AppApiHelper
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

/**
 * @author morad.azzouzi on 12/11/2020.
 */
class MainViewModelTest {

    @Mock
    var retrofitApiService: RetrofitApiService = Mockito.mock(RetrofitApiService::class.java)
    @Mock
    var dao: DaoSearch = Mockito.mock(DaoSearch::class.java)

    @get:Rule
    val rule = TrampolineSchedulerRule()

    private var stateSubscriber: TestObserver<CallStateEnum> = TestObserver.create()
    private lateinit var viewModel: MainViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        val apiHelper: ApiHelper = AppApiHelper(retrofitApiService)
        val dbHelper: DbHelper = AppDbHelper(dao)
        val dataManager: DataManager = AppDataManager(apiHelper, dbHelper)

        val disposables = CompositeDisposable()
        viewModel = MainViewModel(disposables, dataManager)
    }

    @Test
    fun test_getKeywordCount_success() {
        val type = object : TypeToken<Wikipedia>() {}.type
        val bodyResponse: Wikipedia = Gson().fromJson(
            "wikipedia.json",
            javaClass.classLoader!!,
            type
        )
        val response = Response.success(bodyResponse)

        // Given
        Mockito
            .`when`(retrofitApiService.getCount(anyString(), anyString(), anyString(), anyString()))
            .thenReturn(Single.just(response))
        Mockito
            .`when`(
                dao.insert(
                    Search(1L, 1, 2, 3, "toto", "tata", "titi", 123)
                )
            )
            .thenReturn(Single.just(1L))

        // When
        viewModel.state.subscribe(stateSubscriber)
        viewModel.getKeywordCount()
        viewModel.state.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.SUCCESS,
            CallStateEnum.IDLE
        )
    }

    @Test
    fun test_getKeywordCount_error() {
        // Given
        Mockito
            .`when`(retrofitApiService.getCount(anyString(), anyString(), anyString(), anyString()))
            .thenReturn(Single.error(RuntimeException()))

        // When
        viewModel.state.subscribe(stateSubscriber)
        viewModel.getKeywordCount()
        viewModel.state.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.ERROR,
            CallStateEnum.IDLE
        )
    }

    @Test
    fun test_fetchRelatedSearch_success() {
        val type = object : TypeToken<List<Search>>() {}.type
        val listOfSearch: List<Search> = Gson().fromJson(
            "related_search.json",
            javaClass.classLoader!!,
            type
        )

        // Given
        Mockito
            .`when`(dao.fetchRelatedSearch())
            .thenReturn(Single.just(listOfSearch))

        // When
        viewModel.fetchState.subscribe(stateSubscriber)
        viewModel.fetchRelatedSearch()
        viewModel.fetchState.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.SUCCESS,
            CallStateEnum.IDLE
        )
    }

    @Test
    fun test_fetchRelatedSearch_error() {
        // Given
        Mockito
            .`when`(dao.fetchRelatedSearch())
            .thenReturn(Single.error(IllegalAccessException()))

        // When
        viewModel.fetchState.subscribe(stateSubscriber)
        viewModel.fetchRelatedSearch()
        viewModel.fetchState.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.ERROR,
            CallStateEnum.IDLE
        )
    }

    @Test
    fun test_relatedSearchItemSize_success() {
        val type = object : TypeToken<List<Search>>() {}.type
        val listOfSearch: List<Search> = Gson().fromJson(
            "related_search.json",
            javaClass.classLoader!!,
            type
        )

        // Given
        Mockito
            .`when`(dao.fetchRelatedSearch())
            .thenReturn(Single.just(listOfSearch))

        // When
        viewModel.fetchState.subscribe(stateSubscriber)
        viewModel.fetchRelatedSearch()
        viewModel.fetchState.onComplete()

        // Then
        Assert.assertEquals(10, viewModel.relatedSearchItemsSize)
    }
}