package com.mazapps.template.ui.main.viewmodel

import com.mazapps.template.data.db.dao.DaoSearch
import com.mazapps.template.data.model.Search
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

/**
 * @author morad.azzouzi on 14/11/2020.
 */
@HiltAndroidTest
class MainViewModelDatabaseTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var dao: DaoSearch

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    @Throws(Exception::class)
    fun insertSearchAndFetchTest() {
        val id: Long = 1
        val search = Search(
            id,
            12,
            122,
            1222,
            "toto",
            "tata",
            "titi",
            12
        )
        dao.insert(search).blockingGet()
        dao.fetchRelatedSearch().test().assertValue {
            it[0] == search
        }
    }
}