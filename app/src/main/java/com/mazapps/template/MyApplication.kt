package com.mazapps.template

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

/**
 * This generated Hilt component is attached to the Application object's lifecycle and
 * provides dependencies to it. Additionally, it is the parent component of the app,
 * which means that other components can access the dependencies that it provides.
 *
 * @author morad.azzouzi on 11/11/2020.
 */
@HiltAndroidApp
class MyApplication : Application(), Configuration.Provider {

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    override fun getWorkManagerConfiguration(): Configuration {
        return Configuration.Builder()
            .setMinimumLoggingLevel(android.util.Log.INFO)
            .setWorkerFactory(workerFactory)
            .build()
    }
}