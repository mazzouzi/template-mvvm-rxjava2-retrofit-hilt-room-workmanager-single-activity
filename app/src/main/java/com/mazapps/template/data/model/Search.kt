package com.mazapps.template.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "related_search")
data class Search(
    @PrimaryKey(autoGenerate = true)
    var id: Long,
    val ns: Int,
    val pageid: Int,
    val size: Int,
    val snippet: String,
    val timestamp: String,
    val title: String,
    val wordcount: Int
)