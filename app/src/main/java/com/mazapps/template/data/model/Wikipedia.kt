package com.mazapps.template.data.model

data class Wikipedia(
    val batchcomplete: String,
    val `continue`: Continue,
    val query: Query
)