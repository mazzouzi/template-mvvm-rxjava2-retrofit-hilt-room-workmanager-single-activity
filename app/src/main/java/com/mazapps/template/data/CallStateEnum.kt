package com.mazapps.template.data

/**
 * @author morad.azzouzi on 11/11/2020.
 */
enum class CallStateEnum {
    IDLE,
    IN_PROGRESS,
    SUCCESS,
    ERROR,
}