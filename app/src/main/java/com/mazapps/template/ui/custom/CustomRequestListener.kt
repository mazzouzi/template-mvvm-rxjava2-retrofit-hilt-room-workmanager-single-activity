package com.mazapps.tabesto.ui.custom

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.target.Target
import com.mazapps.template.R

/**
 * @author morad.azzouzi on 11/11/2020.
 */
open class CustomRequestListener(
    private val imageView: ImageView,
    private val errorPlaceholderId: Int
) : com.bumptech.glide.request.RequestListener<Drawable> {

    constructor(imageView: ImageView) : this(imageView, R.drawable.image_place_holder)

    override fun onResourceReady(
        resource: Drawable?,
        model: Any?,
        target: Target<Drawable>?,
        dataSource: DataSource?,
        isFirstResource: Boolean
    ): Boolean {
        imageView.setImageDrawable(resource)
        return true
    }

    override fun onLoadFailed(
        e: GlideException?,
        model: Any?,
        target: Target<Drawable>?,
        isFirstResource: Boolean
    ): Boolean {
        imageView.setImageResource(errorPlaceholderId)
        return true
    }
}

