package com.mazapps.template.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.appcompat.app.AppCompatActivity
import androidx.work.*
import androidx.work.ExistingWorkPolicy.APPEND_OR_REPLACE
import com.mazapps.template.R
import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.ui.main.viewmodel.MainViewModel
import com.mazapps.template.worker.WebServiceWorker
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModel: MainViewModel
    @Inject lateinit var disposables: CompositeDisposable

    private val workManager: WorkManager by lazy { WorkManager.getInstance(applicationContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        observeState()
        initView()
    }

    private fun observeState() {
        disposables.add(
            viewModel.state.observeOn(AndroidSchedulers.mainThread()).subscribe {
                when (it) {
                    CallStateEnum.SUCCESS -> onGetKeywordCountSuccess()
                    CallStateEnum.ERROR -> onGetKeywordCountError()
                    else -> {
                        // do nothing
                    }
                }
            }
        )

        disposables.add(
            viewModel.fetchState.observeOn(AndroidSchedulers.mainThread()).subscribe {
                when (it) {
                    CallStateEnum.SUCCESS -> onFetchRelatedSearchSuccess()
                    CallStateEnum.ERROR -> onFetchRelatedSearchError()
                    else -> {
                        // do nothing
                    }
                }
            }
        )
    }

    private fun onGetKeywordCountSuccess() {
        count.text = getString(R.string.count, viewModel.count.toString())
        relatedSearchButton.visibility = View.VISIBLE
    }

    private fun onGetKeywordCountError() {
        initWorker()
        Toast.makeText(this, getString(R.string.error_occured), LENGTH_SHORT).show()
    }

    private fun initWorker() {
        val constraint = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
        val data = Data.Builder().apply { putString(KEYWORD_KEY, viewModel.currentKeyword) }

        val workerRequest = OneTimeWorkRequestBuilder<WebServiceWorker>()
            .setInputData(data.build())
            .setConstraints(constraint)
            .build()

        workManager.enqueueUniqueWork(WORK_NAME, APPEND_OR_REPLACE, workerRequest)
    }

    private fun onFetchRelatedSearchSuccess() {
        Toast.makeText(
            this,
            getString(R.string.count, viewModel.relatedSearchItemsSize.toString()),
            LENGTH_SHORT
        ).show()
    }

    private fun onFetchRelatedSearchError() {
        Toast.makeText(this, getString(R.string.error_occured), LENGTH_SHORT).show()
    }

    private fun initView() {
        keywordCountButton.setOnClickListener { disposables.add(viewModel.getKeywordCount()) }
        relatedSearchButton.setOnClickListener { disposables.add(viewModel.fetchRelatedSearch()) }
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    companion object {
        const val KEYWORD_KEY = "KEYWORD_KEY"
        const val WORK_NAME = "DATA_WS"
    }
}