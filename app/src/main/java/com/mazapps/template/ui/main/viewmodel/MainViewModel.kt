package com.mazapps.template.ui.main.viewmodel

import android.util.Log
import androidx.work.Constraints
import androidx.work.Data
import androidx.work.ExistingWorkPolicy.APPEND_OR_REPLACE
import androidx.work.NetworkType
import androidx.work.WorkManager
import androidx.work.WorkManager.getInstance
import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.data.CallStateEnum.*
import com.mazapps.template.data.DataManager
import com.mazapps.template.data.model.Search
import com.mazapps.template.data.model.Wikipedia
import com.mazapps.template.worker.WebServiceWorker
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Response
import kotlin.random.Random
import androidx.work.OneTimeWorkRequestBuilder as OneTimeWorkRequestBuilder1

/**
 * @author morad.azzouzi on 11/11/2020.
 */
class MainViewModel(
    private val disposables: CompositeDisposable,
    private val dataManager: DataManager
) {

    val fetchState: BehaviorSubject<CallStateEnum> = BehaviorSubject.createDefault(IDLE)
    val state: BehaviorSubject<CallStateEnum> = BehaviorSubject.createDefault(IDLE)

    var count: Int? = null

    private var relatedSearchItems: List<Search>? = null
    val relatedSearchItemsSize: Int
        get() = relatedSearchItems?.size ?: -1

    lateinit var currentKeyword: String

    fun getKeywordCount(): Disposable {
        state.onNext(IN_PROGRESS)
        currentKeyword = getRandomKeyword()
        Log.d("Morad", "keyword = $currentKeyword")

        return dataManager
            .getCount("query", "json", "search", currentKeyword)
            .subscribe(
                { handleGetKeyWordCountResponse(it) },
                { handleGetKeyWordCountError() }
            )
    }

    private fun handleGetKeyWordCountResponse(response: Response<Wikipedia>) {
        val body = response.body()
        if (response.isSuccessful && body != null) {
            handleGetKeyWordCountSuccess(body)
        } else {
            handleGetKeyWordCountError()
        }
    }

    private fun handleGetKeyWordCountSuccess(body: Wikipedia) {
        count = body.query.searchinfo.totalhits
        storeRelatedSearchInDatabase(body)

        state.onNext(SUCCESS)
        state.onNext(IDLE)
    }

    private fun storeRelatedSearchInDatabase(body: Wikipedia) {
        disposables.add(insertRelatedSearch(body.query.search))
    }

    private fun insertRelatedSearch(search: List<Search>): Disposable =
        Observable
            .fromIterable(search)
            .flatMap { dataManager.insert(it).toObservable() }
            .subscribeOn(Schedulers.io())
            .subscribe({}, {})

    private fun handleGetKeyWordCountError() {
        state.onNext(ERROR)
        state.onNext(IDLE)
    }

    fun fetchRelatedSearch(): Disposable {
        fetchState.onNext(IN_PROGRESS)

        return dataManager
            .fetchRelatedSearch()
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    relatedSearchItems = it
                    fetchState.onNext(SUCCESS)
                    fetchState.onNext(IDLE)
                },
                {
                    fetchState.onNext(ERROR)
                    fetchState.onNext(IDLE)
                }
            )
    }

    companion object {
        fun getRandomKeyword(): String {
            val listOfKeyword: List<String> = listOf(
                "sasuke",
                "naruto",
                "sakura",
                "tenten",
                "tobi",
                "shikamaru",
                "kakashi",
                "jiraya",
                "itachi",
                "nagato",
                "orochimaru"
            )
            return listOfKeyword[Random.nextInt(0, 10)]
        }
    }
}