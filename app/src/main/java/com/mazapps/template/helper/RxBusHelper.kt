package com.mazapps.template.helper

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

/**
 * @author morad.azzouzi on 11/11/2020.
 */
object RxBusHelper {

    private val subject = PublishRelay.create<Any>()

    val events: Observable<Any>
        get() = subject

    fun setEvent(event: Any) {
        subject.accept(event)
    }
}