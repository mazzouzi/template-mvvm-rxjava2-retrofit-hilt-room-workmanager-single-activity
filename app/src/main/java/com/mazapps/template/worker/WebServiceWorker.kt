package com.mazapps.template.worker

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.mazapps.template.data.DataManager
import com.mazapps.template.ui.main.view.MainActivity
import io.reactivex.Single

/**
 * @author morad.azzouzi on 14/11/2020.
 */
class WebServiceWorker @WorkerInject constructor (
    @Assisted context: Context,
    @Assisted params: WorkerParameters,
    private val dataManager: DataManager
) : RxWorker(context, params) {

    @SuppressLint("CheckResult")
    override fun createWork(): Single<Result> {
        return dataManager
            .getCount("query", "json", "search", inputData.getString(MainActivity.KEYWORD_KEY))
            .map {
                if (it.isSuccessful) {
                    Log.d(
                        "Morad",
                        "Worker : SUCCESS | keyword = ${inputData.getString(MainActivity.KEYWORD_KEY)}"
                    )
                    Result.success()
                } else {
                    Log.d(
                        "Morad",
                        "Worker : FAILURE | keyword = ${inputData.getString(MainActivity.KEYWORD_KEY)}"
                    )
                    Result.retry()
                }
            }
    }
}