WorkManager a néanmoins quelques limites : 

- si le téléphone est en doze mode, alors le worker sera delayed
- si le téléphone est en mode battery saver (en dessous de 15%), alors le worker sera delayed
- si le téléphone travaille fortement avec plusieurs jobs, plusieurs apps actives etc.. alors le worker sera delayed
- si l'application est forced stop, les workers seront supprimés. Cela dit le système est capable de rescheduler les workers a condition que le workmanager soit initialisé dans l'app.

-> WorkManager est quasi infaillible lorsque couplé à un foreground service. De manière générale, le workmanager est sûr de fonctinner lorsque l'app est en forground et et que les contraintes sont rencontrées.

-> Result.retry() : comme il s'agit d'un "enqueueUniqueWork", si le premier fails alors le retry fait que les workers descendants seront executés si et seulement si le retry fonctionne.

-> Voir la branche "Adding_foreground_service"